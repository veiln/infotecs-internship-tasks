#include <vector>
#include <string>
#include <regex>
#include <iostream>

bool isLonger(std::string a, std::string b) {
    return a.length() > b.length();
}

std::string choose(std::string a, std::string b) {
    if (isLonger(a, b)) {
        return a;
    }

    return b;
}

std::string searchGreedy(std::string s) {
    std::regex expr("[^0-9]+");
    std::smatch matched;
    std::string longestSequence = "";

    while (std::regex_search(s, matched, expr)) {
        longestSequence = choose(matched.str(), longestSequence);
        s = matched.suffix();
    }

    return longestSequence;
}

std::string getLongestSubstring(std::vector<std::string> strings) {
    std::string longestSubstring = "";
    for (std::string &s : strings) {
        longestSubstring = choose(searchGreedy(s), longestSubstring);
    }

    return longestSubstring;
}

std::string reverse(std::string s) {
    std::string result(s);
    std::reverse(result.begin(), result.end()); //no check if the string is a palindrome cause the result of reversing it doesn't change it
    return result;
}

std::string reverseLongestSubstring(std::vector<std::string> strings) {
    std::string s = getLongestSubstring(strings);
    return reverse(s);
}

/* read strings helping functions */

std::vector<std::string> readStrings() {
    size_t n;
    std::cin >> n;

    std::vector<std::string> strings;
    strings.reserve(n);

    std::string s;
    for (int i = 0; i < n; ++i) {
        std::cin >> s;
        strings.push_back(s);
    }

    return strings;
}

int main(int argc, char* argv[]) {

    std::vector<std::string> strings = readStrings();
    std::cout << reverseLongestSubstring(strings) << std::endl;
}

