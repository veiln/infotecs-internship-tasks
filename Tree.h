#include <string>
#include <memory>
#include <vector>

struct Employee {
    std::string name;
    std::string post;
    std::string birthDate;
};

class Node;
using NodePtr = std::shared_ptr<Node>;

class Node {
public:
    Node(Employee &e):e{e}
    {
    }

    void addSubordinate(NodePtr n){
        subordinates.push_back(std::move(n));
    }

private:
    Employee e;
    std::vector<NodePtr> subordinates;
};

struct Tree {
    NodePtr director;
};

