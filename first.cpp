#include <vector>
#include <iostream>
#include <algorithm>

typedef std::vector<size_t> row;
typedef std::vector<row> matrix;

bool isIncreasing(row &r) {
    return std::is_sorted(std::begin(r), std::end(r));
}

bool isDecreasing(row &r) {
    return std::is_sorted(std::begin(r), std::end(r), std::greater<size_t>());
}

bool isMonotonic(row &r) {
    return isIncreasing(r) || isDecreasing(r);
}

void addMonotonic(matrix &m, row &r) {
    if (isMonotonic(r)) {
        m.push_back(r);
    }
}

matrix getMonotonicRows(matrix &m) {
    matrix monotonicRows;

    for (row &r : m) {
        addMonotonic(monotonicRows, r);
    }

    return monotonicRows;
}

/* input and output functions for the matrix type */

row readRow(size_t n) {
    size_t element;
    row r(n, 0);

    for (size_t &elem : r) {
        std::cin >> elem;
    }

    return r;
}

matrix readMatrix() {
    size_t n;
    std::cin >> n;

    matrix m(n, row(n, 0));
    for (row &r : m) {
        r = readRow(n);
    }

    return m;
}

void printRow(row &r) {
    for (size_t &elem : r) {
        std::cout << elem << " ";
    }

    std::cout << std::endl;
}

void print(matrix &m) {
    for (row &r : m) {
        printRow(r);
    }
}


int main(int argc, char * argv[]) {
    matrix m = readMatrix();
    matrix r = getMonotonicRows(m);
    print(r);
}

