#include <string>
#include <sstream>
#include <fstream>
#include "SafeQueue.h"
#include "Tree.h"

class Parser {
public:
    void addToTree(Tree &t, Employee &e) {
        auto eNode = std::make_shared<Node>(e);
        if (e.post == "director") {
            t.director = eNode;
        } else if (e.post == "manager") {
            t.director->addSubordinate(eNode);
            latestManager = eNode;
        } else if (e.post == "ordinary worker") {
            latestManager->addSubordinate(eNode);
        }
    }
    
    void add(Tree &t, std::string s) {
        Employee e = parse(s);
        addToTree(t, e);
    }

    Employee parse(std::string s) {
        std::stringstream ss(s);
        Employee e;
        
        std::getline(ss, e.name, ',');
        std::getline(ss, e.post, ',');
        std::getline(ss, e.birthDate);
        
        return e;
    }
private:
    NodePtr latestManager = nullptr;
};

void print(std::string e) {
    std::cout << e << std::endl;
}

int main() {
    std::ifstream inputFile("input.txt");

    SafeQueue<std::string> q;
    
    std::thread inputThread([&](){
        std::string e;
        while (std::getline(inputFile, e)) {
            q.enqueue(e);
        }
        q.enqueue("ENDOFDATA");
    });
    
    Tree t;

    std::thread outputThread([&](){
        Parser p;
        while(true) {
            std::string e = q.dequeue(); 
            if (e == "ENDOFDATA") {
                break;
            }
            print(e);
            p.add(t, e);
        }
    });
    
    inputThread.join();
    outputThread.join();
    return 0;
}
